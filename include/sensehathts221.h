//
// Created by rschwalk on 01.02.24.
//

#ifndef WS_PI_SENSEHAT_SENSEHATHTS221_H
#define WS_PI_SENSEHAT_SENSEHATHTS221_H

#include <cstdint>

namespace sensehat {

class SenseHatHTS221
{
public:
  SenseHatHTS221();
  ~SenseHatHTS221();

  std::uint8_t read_identifier();

private:
  static constexpr int hts221_addr{ 0x5f };

  std::uint8_t read_byte(int device, std::uint8_t reg);
  void write_byte(int device, uint8_t reg, uint8_t value);
  int m_handle;
};

}// namespace sensehat

#endif// WS_PI_SENSEHAT_SENSEHATHTS221_H
