//
// Created by rschwalk on 29.01.24.
//
#include "sensehathts221.h"
#include <linux/i2c-dev.h>//Needed for I2C port
#include <spdlog/common.h>
#include <spdlog/spdlog.h>

int main()
{
  spdlog::set_level(spdlog::level::debug);

  spdlog::info("Welcome to spdlog");

  sensehat::SenseHatHTS221 sensor{};
  auto identifier = sensor.read_identifier();

  spdlog::info("Identifier of the HTS221: {:#x}", identifier);
}
