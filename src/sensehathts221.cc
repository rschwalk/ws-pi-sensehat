//
// Created by rschwalk on 01.02.24.
//

#include "sensehathts221.h"
#include <fcntl.h>
extern "C" {
#include <i2c/smbus.h>
}
#include <linux/i2c-dev.h>
#include <spdlog/spdlog.h>
#include <sys/ioctl.h>

namespace sensehat {

SenseHatHTS221::SenseHatHTS221() : m_handle(-1)
{
  spdlog::info("HTS221 Sensor will be initialized");
  int adapter_nr = 1; /* probably dynamically determined */
  char rtcname[20];

  snprintf(rtcname, 19, "/dev/i2c-%d", adapter_nr);
  m_handle = open(rtcname, O_RDWR);
  if (m_handle < 0) {
    /* ERROR HANDLING; you can check errno to see what went wrong */
    spdlog::error("Unable to open i2c device for R/W");
  }

  if (ioctl(m_handle, I2C_SLAVE, SenseHatHTS221::hts221_addr) < 0) {
    /* ERROR HANDLING; you can check errno to see what went wrong */
    spdlog::error("Unable to set I2C_SLAVE addr to {}", SenseHatHTS221::hts221_addr);
  }
}

SenseHatHTS221::~SenseHatHTS221() { close(m_handle); }

std::uint8_t SenseHatHTS221::read_byte(int device, std::uint8_t reg)
{
  const auto result = i2c_smbus_read_byte_data(device, reg);
  if (result < 0) { spdlog::error("Error reading byte of data from {} (err result: {})", reg, result); }
  return static_cast<std::uint8_t>(result);
}

void SenseHatHTS221::write_byte(int device, uint8_t reg, uint8_t value)
{
  const auto result = i2c_smbus_write_byte_data(device, reg, value);
  if (result < 0) { spdlog::error("Error writing byte of data reg {} (err result: {})", reg, result); }
}

std::uint8_t SenseHatHTS221::read_identifier() { return read_byte(m_handle, 0xf); }

}// namespace sensehat
